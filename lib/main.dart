import 'dart:core';
import 'package:flutter/material.dart';
import 'package:tack/multitree.dart' as multitree;
import 'package:tack/tree_view.dart';

var nodeList = multitree.build();

void main() {
  runApp(MaterialApp(home: Home()));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TreeView(
        children: getChildList(nodeList[0]!.children),
      )
    );
  }

  List<Widget> getChildList(List<int> children) {
    return children.map((childId) {
      return Container(
        child: TreeViewChild(
          parent: ListTile(title: Text(childId.toString())),
          children: getChildList(nodeList[childId]!.children)
        ),
      );
    }).toList();
  }
}
