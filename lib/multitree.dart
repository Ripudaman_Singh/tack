import 'dart:core';
import 'dart:io';

class InvalidDataException implements Exception {}

class Node {
  String _title;
  List<int> _parents = [];
  List<int> children = [];

  Node(String title) : _title = title;
}

List<Node?> build() {
  List<Node?> nodeList = [];
  final rootDir = Directory("test-tree");

  for (var entity in rootDir.listSync()) {
    final id = _pathToId(entity.path);
    if (id == null) continue;

    final node = Node("");

    final subDir = Directory(entity.path);
    for (var subEntity in subDir.listSync()) {
      final childId = _pathToId(subEntity.path);
      if (childId == null) continue;
      if (subEntity is File) node.children.add(childId);
    }

    if (id + 1 > nodeList.length) {
      for (var i = nodeList.length; i < id; i++) nodeList.add(null);
      nodeList.add(node);
    } else if (nodeList[id] != null) {
      throw InvalidDataException();
    } else {
      nodeList[id] = node;
    }
  }

  for (var id = 0; id < nodeList.length; id++) {
    final parentNode = nodeList[id];
    if (parentNode == null) continue;
    for (var childId in parentNode.children) {
      final childNode = nodeList[childId];
      if (childNode == null) throw InvalidDataException();

      childNode._parents.add(id);
    }
  }

  return nodeList;
}

int? _pathToId(String path) {
  return int.tryParse(path.split('/').last);
}
